package com.udacity.stockhawk;

/**
 * This is an event class.
 */
public class CustomErrorMessageEvent {

    private String customErrorMessage;

    public String getCustomErrorMessage() {
        return customErrorMessage;
    }

    public void setCustomErrorMessage(String customErrorMessage) {
        this.customErrorMessage = customErrorMessage;
    }



}
