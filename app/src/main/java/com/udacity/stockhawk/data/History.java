package com.udacity.stockhawk.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import yahoofinance.histquotes.HistoricalQuote;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by Anu on 4/10/17.
 */


public class History implements Parcelable{

    String historyMonthly;
    String historyQuarterly;
    String historyHalfYearly;

    public History(){
       historyMonthly = null;
       historyQuarterly = null;
       historyHalfYearly = null;


    }

    // Using the `in` variable, we can retrieve the values that
    // we originally wrote into the `Parcel`.  This constructor is usually
    // private so that only the `CREATOR` field can access.
    private History(Parcel in) {
        historyHalfYearly = in.readString();
        historyQuarterly = in.readString();
        historyMonthly = in.readString();
    }
    public History(String historyHalfYearly, String historyQuarterly,String historyMonthly) {
        this.historyMonthly = historyMonthly;
        this.historyQuarterly = historyQuarterly;
        this.historyHalfYearly = historyHalfYearly;
    }

    public String getHistoryMonthly() {
        return historyMonthly;
    }

    public String getHistoryQuarterly() {
        return historyQuarterly;
    }

    public String getHistoryHalfYearly() { return  historyHalfYearly;}

    public void setHistoryMonthly(String historyMonthly) {
        this.historyMonthly = historyMonthly;
    }

    public void setHistoryQuarterly(String historyQuarterly) {
        this.historyQuarterly = historyQuarterly;
    }

    public void setHistoryHalfYearly(String historyHalfYearly){
        this.historyHalfYearly = historyHalfYearly;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    // This is where you write the values you want to save to the `Parcel`.
    // The `Parcel` class has methods defined to help you save all of your values.

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(historyHalfYearly);
        parcel.writeString(historyQuarterly);
        parcel.writeString(historyMonthly);
    }

    // `Parcelable.Creator<History> CREATOR` constant for our class;
    // Notice how it has our class specified as its type.
    public static final Parcelable.Creator<History> CREATOR
            = new Parcelable.Creator<History>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public History createFromParcel(Parcel in) {
            return new History(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public History[] newArray(int size) {
            return new History[size];
        }
    };

}
