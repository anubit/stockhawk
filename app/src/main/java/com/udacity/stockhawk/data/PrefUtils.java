package com.udacity.stockhawk.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.udacity.stockhawk.R;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public final class PrefUtils {

    public static String LOG_TAG = PrefUtils.class.getSimpleName();
    private PrefUtils() {
    }

    public static Set<String> getStocks(Context context) {

        Log.d(LOG_TAG,"inside getStocks()");
        String stocksKey = context.getString(R.string.pref_stocks_key);
        String initializedKey = context.getString(R.string.pref_stocks_initialized_key);
        String[] defaultStocksList = context.getResources().getStringArray(R.array.default_stocks);
        Log.d(LOG_TAG,"default stocks list = " + defaultStocksList.length);


        // create a HashSet of messages
        HashSet<String> defaultStocks = new HashSet<>(Arrays.asList(defaultStocksList));
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);


        boolean initialized = prefs.getBoolean(initializedKey, false);

        if (!initialized) {
            Log.d(LOG_TAG,"initialize the default stocks");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(initializedKey, true);
            // loading the default stocks in shared pref
            editor.putStringSet(stocksKey, defaultStocks);
            editor.apply();
            return defaultStocks;
        }
        Log.d(LOG_TAG,"return this stock set - default stock the first time");
        return prefs.getStringSet(stocksKey, new HashSet<String>());

    }

    private static void editStockPref(Context context, String symbol, Boolean add) {

        Log.d(LOG_TAG,"inside edit stock pref function");
        String key = context.getString(R.string.pref_stocks_key);
        Log.d(LOG_TAG,"getStocks");
        // get the default stock list
        Set<String> stocks = getStocks(context);

        Log.d(LOG_TAG,"add the new symbol to the set and put that in shared pref" + symbol);

        if (add) {
            stocks.add(symbol);
        } else {
            stocks.remove(symbol);
        }

        // add the new modified stock set to pref
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putStringSet(key, stocks);
        editor.apply();
    }

    public static void addStock(Context context, String symbol) {
        editStockPref(context, symbol, true);
    }

    public static void removeStock(Context context, String symbol) {
        editStockPref(context, symbol, false);
    }

    // storing the display mode $ or % in shared pref
    public static String getDisplayMode(Context context) {
        String key = context.getString(R.string.pref_display_mode_key);
        String defaultValue = context.getString(R.string.pref_display_mode_default);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Log.d(LOG_TAG,"get the prefs key stored before: " + prefs.getString(key,defaultValue).toString());
        return prefs.getString(key, defaultValue);
    }

    // Anu : Added this new static method to get the status of the stocks
  /*  @SuppressWarnings("ResourceType")
    static public @StockSyncAdapter.StockStatus
    int getStockStatus(Context c){
         SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
         return prefs.getInt(c.getString(R.string.pref_stocks_key),
            StockSyncAdaptr.STOCK_STATUS_UNKNOW);
    }*/

    public static void toggleDisplayMode(Context context) {

        String key = context.getString(R.string.pref_display_mode_key);
        String absoluteKey = context.getString(R.string.pref_display_mode_absolute_key);
        String percentageKey = context.getString(R.string.pref_display_mode_percentage_key);

        Log.d(LOG_TAG,"inside toggle display mode ");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String displayMode = getDisplayMode(context);

        Log.d(LOG_TAG,"get the display mode and put it in shared pref");
        SharedPreferences.Editor editor = prefs.edit();

        if (displayMode.equals(absoluteKey)) {
            editor.putString(key, percentageKey);
        } else {
            editor.putString(key, absoluteKey);
        }

        editor.apply();
    }


    public static String getStringDateFromMillisec(long xDateValue) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(xDateValue);
        Log.d(LOG_TAG, "date = " + date);
        return date;
    }

    public static String getFormattedStringDateFromMillisec(long xDateValue){
        String pattern = "EEE MMM yyyy hh:mm:ss.Z";
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(xDateValue);
        System.out.println(date);
        return date;
    }

    public static void convertTimeStampToSmallNumbers(){

    }
}
