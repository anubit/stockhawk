package com.udacity.stockhawk.sync;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.udacity.stockhawk.CustomErrorMessageEvent;
import com.udacity.stockhawk.R;
import com.udacity.stockhawk.data.Contract;
import com.udacity.stockhawk.data.History;
import com.udacity.stockhawk.data.PrefUtils;
import com.udacity.stockhawk.ui.MainActivity;
import com.udacity.stockhawk.ui.StockAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.logging.LogRecord;

import timber.log.Timber;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.stock.StockQuote;

import static android.R.attr.handle;
import static android.R.attr.name;
import static android.media.CamcorderProfile.get;
import static android.os.Looper.getMainLooper;
import static java.lang.Boolean.TRUE;

public final class QuoteSyncJob {

    public static final String LOG_TAG = QuoteSyncJob.class.getSimpleName();
    public static boolean GETSTOCK = false;
    private static final int ONE_OFF_ID = 2;
    private static final String ACTION_DATA_UPDATED = "com.udacity.stockhawk.ACTION_DATA_UPDATED";
    private static final int PERIOD = 300000;
    private static final int INITIAL_BACKOFF = 10000;
    private static final int PERIODIC_ID = 1;
    private static final int YEARS_OF_HISTORY = 1;
    private Handler mHandler;
    public static History historyObjectModel = null;

    private QuoteSyncJob() {

    }

    // This is called by QuoteIntentService and when you add a new stock
    static void getQuotes(Context context) {

        Log.d(LOG_TAG, "inside getQUotes function in QuoteSyncJob class");
        Timber.d("Running sync job");

        // creating calendar instance that gives the current date and time
        Calendar from = Calendar.getInstance();
        // from.add(Calendar.YEAR, -YEARS_OF_HISTORY);
        // get 6 months of stock quotes
        from.add(Calendar.MONTH, - 6);
        Log.d(" from calendar: ", from.toString());

        Calendar to = Calendar.getInstance();
        Calendar quarter = Calendar.getInstance();
        quarter.add(Calendar.MONTH, -3);

        // get the stocks from pref
        Set<String> stockPref = PrefUtils.getStocks(context);
        Log.d(LOG_TAG, "get the stocks from the preferences");
        Set<String> stockCopy = new HashSet<>();
        // stock copy is a copy from prefUtils
        stockCopy.addAll(stockPref);
        Log.d(LOG_TAG, "make a copy of prefUtils in stockCopy set");
        String[] stockArray = stockPref.toArray(new String[stockPref.size()]);

        Timber.d(stockCopy.toString());

        if (stockArray.length == 0) {
            return;
        }

        // get the stocks (loaded in pref) from YahooFinance API
        // Stock are added to the stock set which in turn added to pref
        Map<String, Stock> quotes = null;
        try {
            quotes = YahooFinance.get(stockArray);
        } catch (IOException e) {
            Log.d(LOG_TAG, "invalid stock symbol");
        } catch (NullPointerException e) {
            Log.d(LOG_TAG, " stock array is empty, add new stocks for display");
        }
        Iterator<String> iterator = stockCopy.iterator();

        Timber.d(quotes.toString());


        // Add the preloaded stocks to database
        ArrayList<ContentValues> quoteCVs = null;
        while (iterator.hasNext()) {
            String symbol = iterator.next();
            Stock stock = quotes.get(symbol);

            // The name is used for display
            String nameOfStock = stock.getName();
            if (stock != null && stock.getName() != null) {
                Log.d(LOG_TAG, "stock is not null");
                GETSTOCK = true;
            } else GETSTOCK = false;

            // Handling the invalid symbol
            //1. remove the quote from the iterator
            // 2. send the error message to UI
            // 3. remove this quote from the prefUtils as it gets added

            if (GETSTOCK == false) {
                Log.d(LOG_TAG, "move to next stock ");
                iterator.remove();
                // sendErrorMessageToUI(context);
                CustomErrorMessageEvent event = new CustomErrorMessageEvent();
                event.setCustomErrorMessage(context.getString(R.string.invalid_stock_symbol));
                org.greenrobot.eventbus.EventBus.getDefault().post(event);
                PrefUtils.removeStock(context, symbol);
                continue;
            } else if (GETSTOCK)  //  GETSTOCK = true, get the stock info {

                Log.d(LOG_TAG, "stock not null, get quotes");
            StockQuote quote = stock.getQuote();
            Log.d(LOG_TAG, " stock quote = " + quote.toString());

            float price = quote.getPrice().floatValue();
            float change = quote.getChange().floatValue();
            float percentChange = quote.getChangeInPercent().floatValue();

            //convert big decimal to int to store it in sqlite
            BigDecimal prevClose = quote.getPreviousClose(); //1234.5678
            Log.d(LOG_TAG, "prev close in decimal = " + prevClose);
            int prevCloseInt = prevClose.scaleByPowerOfTen(2).intValue();
            // it will converted to cents.

            BigDecimal open = quote.getOpen();
            int openInt = open.scaleByPowerOfTen(4).intValue();
            BigDecimal dayHigh = quote.getDayHigh();
            int dayHighInt = dayHigh.scaleByPowerOfTen(4).intValue();
            BigDecimal dayLow = quote.getDayLow();
            int dayLowInt = dayHigh.scaleByPowerOfTen(4).intValue();
            BigDecimal yearHigh = quote.getYearHigh();
            int yearHighInt = dayHigh.scaleByPowerOfTen(4).intValue();
            BigDecimal yearLow = quote.getYearLow();
            int yearLowInt = dayHigh.scaleByPowerOfTen(4).intValue();


            // WARNING! Don't request historical data for a stock that doesn't exist!
            // The request will hang forever X_x
            List<HistoricalQuote> history_weekly = null;
            List<HistoricalQuote> history_half_yearly = null; // remove this later
            List<HistoricalQuote> history_quarterly = new ArrayList<HistoricalQuote>();
            List<HistoricalQuote> history_monthly = new ArrayList<HistoricalQuote>();
            quoteCVs = new ArrayList<>();
            try {
                // getting the history
               // history_weekly =
                   //     stock.getHistory(from, to, Interval.WEEKLY);

                // Get history data 6 months ago from today by weekly - one time call
                history_half_yearly = stock.getHistory(from,Interval.WEEKLY);

                // for each quote compare the date and create another new list
                Date pastDate = quarter.getTime();
                System.out.println(" the past date  = " + pastDate);

                // extracting monthly, quarterly data from half_yearly instead of calling the
                // api again
                for (HistoricalQuote h: history_half_yearly){

                    Calendar calHistory = h.getDate();
                    Date calHistoryDate = calHistory.getTime();
                    System.out.println(" the date d = " + calHistoryDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
                    System.out.println("The date is: " + sdf.format(calHistoryDate));
                    // creating a list for monthly
                    if (calHistoryDate.compareTo(pastDate) == 0 ){
                        history_monthly.add(h);
                    }
                    // creating a list for quarterly
                    else if (calHistoryDate.compareTo(pastDate) > 0){
                        history_quarterly.add(h);
                    }
                }

                // show the 3 month history dates by week
                System.out.println("history by quartely size = " + history_quarterly.size() );
                for(HistoricalQuote hs: history_quarterly) {
                    Log.d(LOG_TAG, " quarterly quote  list = " + hs);
                }

                System.out.println("history by monthly size = " + history_monthly.size() );
                for(HistoricalQuote hs: history_monthly) {
                    Log.d(LOG_TAG, " monthly quote  list = " + hs);
                }



            } catch (IOException exception) {
                Timber.e(exception, "Error fetching stock quotes");

            }

            // I have downloaded from yahoo finance, one year worth of history.
            // history_weekly is a string, where the interval is by week.
            //  How to avoid having 3 strings in my database?

            // history info is stored as a string in database - here we are storing 6 months of data
            StringBuilder historyHalfYearly = new StringBuilder();
            for (HistoricalQuote it : history_half_yearly) {
                // getting the date in millisec
                long dateInMillis = it.getDate().getTimeInMillis();
                historyHalfYearly.append(dateInMillis);
                // storing the date as string
                      /*  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String date = dateFormat.format(it.getDate().getTime());
                        historyBuilder.append(date);*/
                historyHalfYearly.append(", ");
                historyHalfYearly.append(it.getClose());
                historyHalfYearly.append("\n");
            }

            Log.d(LOG_TAG, " history half yearly size = " + history_half_yearly.size());
            // create a string for quartely
            // Extracting date and closing price
            StringBuilder historyQuartely = new StringBuilder();
            for (HistoricalQuote it : history_half_yearly) {
                // getting the date in millisec
                long dateInMillis = it.getDate().getTimeInMillis();
                historyQuartely.append(dateInMillis);
                historyQuartely.append(", ");
                historyQuartely.append(it.getClose());
                historyQuartely.append("\n");
            }

            Log.d(LOG_TAG, " history quarterly size = " + history_quarterly.size());

            StringBuilder historyMonthly = new StringBuilder();
            for (HistoricalQuote it : history_half_yearly) {
                // getting the date in millisec
                long dateInMillis = it.getDate().getTimeInMillis();
                historyMonthly.append(dateInMillis);
                historyMonthly.append(", ");
                historyMonthly.append(it.getClose());
                historyMonthly.append("\n");
            }

            Log.d(LOG_TAG, " history Monthly size = " + history_monthly.size());

            // creating history model for quartely, halfyearly, monthly data
            historyObjectModel= new History(historyHalfYearly.toString(),
                    history_quarterly.toString(),historyMonthly.toString());

            ContentValues quoteCV = new ContentValues();
            quoteCV.put(Contract.Quote.COLUMN_SYMBOL, symbol);
            quoteCV.put(Contract.Quote.COLUMN_PRICE, price);
            quoteCV.put(Contract.Quote.COLUMN_PERCENTAGE_CHANGE, percentChange);
            quoteCV.put(Contract.Quote.COLUMN_ABSOLUTE_CHANGE, change);
            quoteCV.put(Contract.Quote.COLUMN_PREV_CLOSE, prevCloseInt);
            quoteCV.put(Contract.Quote.COLUMN_OPEN, openInt);
            quoteCV.put(Contract.Quote.COLUMN_HIGH, dayHighInt);
            quoteCV.put(Contract.Quote.COLUMN_LOW, dayLowInt);
            quoteCV.put(Contract.Quote.COLUMN_YEAR_HIGH, yearHighInt);
            quoteCV.put(Contract.Quote.COLUMN_YEAR_LOW, yearLowInt);
            quoteCV.put(Contract.Quote.COLUMN_NAME, nameOfStock);
            quoteCV.put(Contract.Quote.COLUMN_HISTORY, historyHalfYearly.toString());

            quoteCVs.add(quoteCV);
        }


        // bulk insert from quoteCVs to contentValues
        context.getContentResolver()
                .bulkInsert(
                        Contract.Quote.URI,
                        quoteCVs.toArray(new ContentValues[quoteCVs.size()]));

        Intent dataUpdatedIntent = new Intent(ACTION_DATA_UPDATED);
        context.sendBroadcast(dataUpdatedIntent);

    }


    // Add a snack bar to show the invalid stock symbol
    public static void sendErrorMessageToUI(final Context context) {

        new Handler(getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(context, R.string.invalid_stock_symbol, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static void schedulePeriodic(Context context) {
        Timber.d("Scheduling a periodic task");


        JobInfo.Builder builder = new JobInfo.Builder(PERIODIC_ID, new ComponentName(context, QuoteJobService.class));


        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPeriodic(PERIOD)
                .setBackoffCriteria(INITIAL_BACKOFF, JobInfo.BACKOFF_POLICY_EXPONENTIAL);


        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        scheduler.schedule(builder.build());
    }


    public static synchronized void initialize(final Context context) {

        schedulePeriodic(context);
        syncImmediately(context);

    }

    public static synchronized void syncImmediately(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            Intent nowIntent = new Intent(context, QuoteIntentService.class);
            context.startService(nowIntent);
        } else {

            JobInfo.Builder builder = new JobInfo.Builder(ONE_OFF_ID, new ComponentName(context, QuoteJobService.class));


            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setBackoffCriteria(INITIAL_BACKOFF, JobInfo.BACKOFF_POLICY_EXPONENTIAL);


            JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

            scheduler.schedule(builder.build());


        }
    }


}
