package com.udacity.stockhawk.utility;

import android.util.Log;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.udacity.stockhawk.data.PrefUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Anu on 3/8/17.
 */

// This class allows custom-format values on the axis
// ref: https://github.com/PhilJay/MPAndroidChart/wiki/The-AxisValueFormatter-interface
public class MyXAxisValueFormatter implements IAxisValueFormatter {
    //private long[] mValues;

    private SimpleDateFormat dateFormat;

    public static String LOG_TAG = MyXAxisValueFormatter.class.getSimpleName();
    public MyXAxisValueFormatter() {
       dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        // "value" represents the position of the label on the axis (x or y)
        Log.d(LOG_TAG,"value = " + value);
        Log.d(LOG_TAG, " date format = " + dateFormat.format(value));
        return dateFormat.format(value).toString();
    }

    public int getDecimalDigits() {
        return 0;
    }
}
