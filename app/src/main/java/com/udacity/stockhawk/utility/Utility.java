package com.udacity.stockhawk.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.udacity.stockhawk.R;
import com.udacity.stockhawk.ui.StockAdapter;


import static com.udacity.stockhawk.ui.StockAdapter.*;

/**
 * Created by Anu on 2/16/17.
 */

public  class Utility {

    public static boolean isNetworkAvailable(Context c){
        ConnectivityManager cm = (ConnectivityManager) c.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

    }


}
