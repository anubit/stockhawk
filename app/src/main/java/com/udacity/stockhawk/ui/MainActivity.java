package com.udacity.stockhawk.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.udacity.stockhawk.CustomErrorMessageEvent;
import com.udacity.stockhawk.R;
import com.udacity.stockhawk.data.Contract;
import com.udacity.stockhawk.data.PrefUtils;
import com.udacity.stockhawk.sync.QuoteSyncJob;
import com.udacity.stockhawk.utility.Utility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static android.R.id.message;
import static com.github.mikephil.charting.charts.Chart.LOG_TAG;
import static com.udacity.stockhawk.R.id.error;
import static com.udacity.stockhawk.R.id.recycler_view;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;
import static com.udacity.stockhawk.R.id.tool_bar;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        SwipeRefreshLayout.OnRefreshListener, SharedPreferences.OnSharedPreferenceChangeListener,
        StockAdapter.StockAdapterOnClickHandler {

    private static final int STOCK_LOADER = 0;
    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.recycler_view)
    RecyclerView stockRecyclerView;
    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @SuppressWarnings("WeakerAccess")
    @BindView(error)
    TextView errorView;
    @BindView(tool_bar)
    Toolbar toolbar;
    private StockAdapter adapter;

    private String errorMessage;



    @Override
    public void onClick(String symbol) {
        Timber.d("Symbol clicked: %s", symbol);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG,"main activity");
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // make the action bar as tool bar
        setSupportActionBar(toolbar);
        // Anu: I think this is to get the caret button up
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // setDisplayShowTitleEnabled(false);

        getSupportActionBar().setElevation(2f); // change it to of

        // try setHomeButtonEnabled
        // setDisplayShowHomeEnabled

        adapter = new StockAdapter(this, this);


        stockRecyclerView.setAdapter(adapter);
        stockRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);


        // register this class with the event bus
        org.greenrobot.eventbus.EventBus.getDefault().register(this);


        QuoteSyncJob.initialize(this);
        onRefresh();
        getSupportLoaderManager().initLoader(STOCK_LOADER, null, this);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                String symbol = adapter.getSymbolAtPosition(viewHolder.getAdapterPosition());
                PrefUtils.removeStock(MainActivity.this, symbol);
                getContentResolver().delete(Contract.Quote.makeUriForStock(symbol), null, null);
            }
        }).attachToRecyclerView(stockRecyclerView);


    }

    // Add strings in strings.xml
    // TODO: clean up this code. too much redundancy
    @Override
    public void onRefresh() {

        QuoteSyncJob.syncImmediately(this);
        int message;
        Log.d(LOG_TAG,"onRefresh()");
        // network not  available and server down
        if (!Utility.isNetworkAvailable(getBaseContext()) && adapter.getItemCount() == 0) {
            swipeRefreshLayout.setRefreshing(false);
            message = R.string.empty_stock_info_server_down;
            errorView.setText(message);
            errorView.setVisibility(View.VISIBLE);

        } else
        // network available, invalid stock symbol
        if (Utility.isNetworkAvailable(getBaseContext()) && adapter.getItemCount() == 0) {
            swipeRefreshLayout.setRefreshing(false);
            message = R.string.invalid_stock_symbol;
            errorView.setText(message);
            errorView.setVisibility(View.VISIBLE);

        } // no network available
        else if (!Utility.isNetworkAvailable(getBaseContext())) {
            swipeRefreshLayout.setRefreshing(false);
            message = R.string.error_no_network;
            Toast.makeText(this, R.string.toast_no_connectivity, Toast.LENGTH_LONG).show();
            errorView.setText(message);
            errorView.setVisibility(View.VISIBLE);
            stockRecyclerView.setVisibility(View.GONE);

        } // no stocks added in the server
         else if (PrefUtils.getStocks(this).size() == 0) {
            swipeRefreshLayout.setRefreshing(false);
            message = R.string.error_no_stocks_added;
            errorView.setText(message);
            errorView.setVisibility(View.VISIBLE);

        }else {
            errorView.setVisibility(View.GONE);
            stockRecyclerView.setVisibility(View.VISIBLE);
           
        }

    }

    public void button(@SuppressWarnings("UnusedParameters") View view) {
        new AddStockDialog().show(getFragmentManager(), "StockDialogFragment");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CustomErrorMessageEvent event){
        Log.d(LOG_TAG,"event fired " + event.getCustomErrorMessage());
        Toast.makeText(this,event.getCustomErrorMessage(),Toast.LENGTH_SHORT).show();
     //   Toast.makeText(this, R.string.invalid_stock_symbol, Toast.LENGTH_SHORT).show();
        errorMessage = event.getCustomErrorMessage();
        errorView.setText(errorMessage);
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    // Added the network available check.
    void addStock(String symbol) {

        if (symbol != null && !symbol.isEmpty() ) {
            if (Utility.isNetworkAvailable(getBaseContext())) {
                swipeRefreshLayout.setRefreshing(true);
            } else {
                Log.d(LOG_TAG,"add stock method");
                String message = getString(R.string.toast_stock_added_no_connectivity, symbol);
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                errorView.setText(message);
                errorView.setVisibility(View.VISIBLE);
            }

            PrefUtils.addStock(this, symbol);
            QuoteSyncJob.syncImmediately(this);
        }
        else { // The user press the add button without entering any symbol
            Log.d(LOG_TAG,"enter valid symbol");
            String message = getString(R.string.enter_valid_symbol);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
          //  errorView.setText(message);
           // errorView.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                Contract.Quote.URI,
                Contract.Quote.QUOTE_COLUMNS.toArray(new String[]{}),
                null, null, Contract.Quote.COLUMN_SYMBOL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    protected void onPause() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.unregisterOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        swipeRefreshLayout.setRefreshing(false);

        // Anu: set the view to textview if data count is 0.
        if (data.getCount() == 0){
            stockRecyclerView.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
        }
        else {
            stockRecyclerView.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.GONE);
        }
        adapter.setCursor(data);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        swipeRefreshLayout.setRefreshing(false);
        adapter.setCursor(null);
    }


    private void setDisplayModeMenuItemIcon(MenuItem item) {
        if (PrefUtils.getDisplayMode(this)
                .equals(getString(R.string.pref_display_mode_absolute_key))) {
            item.setIcon(R.drawable.ic_percentage);
        } else {
            item.setIcon(R.drawable.ic_dollar);
        }
    }

    // notice we don;t have settings acitivty here
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_settings, menu);
        MenuItem item = menu.findItem(R.id.action_change_units);
        setDisplayModeMenuItemIcon(item);
        return true;
    }

    // the user preference options are dealt immediately here, instead
    // of setting activity with prefChangedListener
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_change_units) {
            PrefUtils.toggleDisplayMode(this);
            setDisplayModeMenuItemIcon(item);
            adapter.notifyDataSetChanged();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(LOG_TAG," on shared pref changed ");
        if (key.equals("stocks")) {
            QuoteSyncJob.syncImmediately(this);
        }

    }
}
