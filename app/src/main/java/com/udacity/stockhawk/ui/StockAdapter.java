package com.udacity.stockhawk.ui;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.IntDef;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.udacity.stockhawk.R;
import com.udacity.stockhawk.data.Contract;
import com.udacity.stockhawk.data.PrefUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.security.AccessController.getContext;

// Recycler view adapter that
public class StockAdapter extends RecyclerView.Adapter<StockAdapter.StockViewHolder> {

    private final Context context;
    private final DecimalFormat dollarFormatWithPlus;
    private final DecimalFormat dollarFormat;
    private final DecimalFormat percentageFormat;
    private Cursor cursor;
    private final StockAdapterOnClickHandler clickHandler;
    private int mPosition = RecyclerView.NO_POSITION;

    // Anu  Added Annotation  to deal with no data or corrupted data when the server is down
    // or with the issue of captive portal
    // using to display empty text view in main activity
    @Retention(RetentionPolicy.SOURCE)

    @IntDef({STATUS_OK,STATUS_SERVER_DOWN,STATUS_SERVER_INVALID,STATUS_UNKNOWN,STATUS_INVALID})
    public @interface  ServerStatus{}
    public static final int STATUS_OK = 0;
    public static final int STATUS_SERVER_DOWN = 1;
    public static final int STATUS_SERVER_INVALID = 2;
    public static final int STATUS_UNKNOWN = 3;
    public static final int STATUS_INVALID = 4;

    public static String LOG_TAG = StockAdapter.class.getSimpleName();
    // contructor will receive an object that implements this interface
    StockAdapter(Context context, StockAdapterOnClickHandler clickHandler) {
        this.context = context;
        // passing the listener in the constructor
        this.clickHandler = clickHandler;

        dollarFormat = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.US);
        dollarFormatWithPlus = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.US);
        dollarFormatWithPlus.setPositivePrefix("+$");
        percentageFormat = (DecimalFormat) NumberFormat.getPercentInstance(Locale.getDefault());
        percentageFormat.setMaximumFractionDigits(2);
        percentageFormat.setMinimumFractionDigits(2);
        percentageFormat.setPositivePrefix("+");
    }

    public Context getContext(){
        return context;
    }

    // this swaps the cursor
    void setCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }

    String getSymbolAtPosition(int position) {

        cursor.moveToPosition(position);
        return cursor.getString(Contract.Quote.POSITION_SYMBOL);
    }

    @Override
    public StockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(context).inflate(R.layout.list_item_quote, parent, false);

        return new StockViewHolder(item);
    }

    // onBindViewHolder is called each time when the user scrolls to bind the data with the view.
    //it is not a good idea to call the bind method to attach the listener
    @Override
    public void onBindViewHolder(StockViewHolder holder, int position) {

        cursor.moveToPosition(position);


        holder.symbol.setText(cursor.getString(Contract.Quote.POSITION_SYMBOL));
        holder.price.setText(dollarFormat.format(cursor.getFloat(Contract.Quote.POSITION_PRICE)));


        float rawAbsoluteChange = cursor.getFloat(Contract.Quote.POSITION_ABSOLUTE_CHANGE);
        float percentageChange = cursor.getFloat(Contract.Quote.POSITION_PERCENTAGE_CHANGE);

        if (rawAbsoluteChange > 0) {
            holder.change.setBackgroundResource(R.drawable.percent_change_pill_green);
        } else {
            holder.change.setBackgroundResource(R.drawable.percent_change_pill_red);
        }

        String change = dollarFormatWithPlus.format(rawAbsoluteChange);
        String percentage = percentageFormat.format(percentageChange / 100);

        if (PrefUtils.getDisplayMode(context)
                .equals(context.getString(R.string.pref_display_mode_absolute_key))) {
            holder.change.setText(change);
        } else {
            holder.change.setText(percentage);
        }


    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (cursor != null) {
            count = cursor.getCount();
        }
        return count;
    }

    // Interface for click handler for recycler view.
    // This interface is called in view's setOnClickListener
    interface StockAdapterOnClickHandler {
        void onClick(String symbol);
    }

    // view holder object can get hold of the listener
    class StockViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.symbol)
        TextView symbol;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.change)
        TextView change;

        public StockViewHolder(View itemView) {
            super(itemView);

            // setting the listener to the view holder object instead of doing
            // in onBindViewHolder.
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        // on click of each stock, make the uri and pass the stock symbol to the detail activity
        @Override
        public void onClick(View v) {


            mPosition = getAdapterPosition();
            Toast.makeText(context,"onClick clicked, adapter position",Toast.LENGTH_SHORT).show();
            cursor.moveToPosition(mPosition);
            int symbolColumn = cursor.getColumnIndex(Contract.Quote.COLUMN_SYMBOL);
            Log.d(LOG_TAG, "symbol = " + cursor.getString(symbolColumn));
            String symbol = cursor.getString(symbolColumn);
            // Calling the interface implementation onClick.
            // we are passing the symbol to main activity
            clickHandler.onClick(cursor.getString(symbolColumn));

            Uri uri = Contract.Quote.makeUriForStock(symbol);

            if (cursor != null) {
                Log.d(LOG_TAG, "passing this uri to detail fragment " +
                        Contract.Quote.makeUriForStock(symbol));

                // calling the DetailActivity class with the movie id uri
                Intent intent = new Intent(context, DetailActivity.class)
                        .setData(uri);
                context.startActivity(intent);


            }

        }

    }
}
