package com.udacity.stockhawk.ui;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.udacity.stockhawk.R;
import com.udacity.stockhawk.data.Contract;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.udacity.stockhawk.R.id.tool_bar;
import static com.udacity.stockhawk.R.id.tv_52week_high_value;
import static com.udacity.stockhawk.R.id.tv_52week_low_value;
import static com.udacity.stockhawk.R.id.tv_current_price;
import static com.udacity.stockhawk.R.id.tv_day_high_value;
import static com.udacity.stockhawk.R.id.tv_day_low_value;
import static com.udacity.stockhawk.R.id.tv_name_symbol;
import static com.udacity.stockhawk.R.id.tv_open_value;
import static com.udacity.stockhawk.R.id.tv_prev_close_value;

/**
 * Created by Anu on 3/2/17.
 *
 * This displays symbol, price and history in chart form
 */

public class DetailActivity extends AppCompatActivity
    implements android.app.LoaderManager.LoaderCallbacks<Cursor> {


    public static String LOG_TAG = DetailActivity.class.getSimpleName();
    private static final int DETAIL_WEEK_LOADER = 1;


    private Uri mUri;
    private HistoryFragmentPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    // The callbacks through which we will interact with the LoaderManager.
    private android.app.LoaderManager.LoaderCallbacks<Cursor> mCallbacks;
    private HistoryFragment historyFragment, viewPagerFragment;


    public static final int POSITION_SYMBOL = 0;
    public static final int POSITION_PRICE = 1;
    public static final int POSITION_ABSOLUTE_CHANGE =2;
    public static final int POSITION_PERCENTAGE_CHANGE = 3;
    public static final int POSITION_PREV_CLOSE = 4;
    public static final int POSITION_OPEN = 5;
    public static final int POSITION_DAY_HIGH = 6;
    public static final int POSITION_DAY_LOW = 7;
    public static final int POSITION_YEAR_HIGH = 8;
    public static final int POSITION_YEAR_LOW = 9;
    public static final int POSITION_HISTORY = 10;
    public static final int POSITION_NAME = 11;


    private static final String[] DETAIL_COLUMNS = {
            Contract.Quote.TABLE_NAME + "." +
                    Contract.Quote.COLUMN_SYMBOL,
            Contract.Quote.COLUMN_PRICE,
            Contract.Quote.COLUMN_ABSOLUTE_CHANGE,
            Contract.Quote.COLUMN_PERCENTAGE_CHANGE,
            Contract.Quote.COLUMN_PREV_CLOSE,
            Contract.Quote.COLUMN_OPEN,
            Contract.Quote.COLUMN_HIGH,
            Contract.Quote.COLUMN_LOW,
            Contract.Quote.COLUMN_YEAR_HIGH,
            Contract.Quote.COLUMN_YEAR_LOW,
            Contract.Quote.COLUMN_HISTORY,
            Contract.Quote.COLUMN_NAME
    };

    @BindView(tool_bar)
    Toolbar toolbar;
    @BindView(tv_name_symbol)
    TextView tvNameSymbol;
    @BindView(tv_current_price)
    TextView tvCurrentPrice;
    @BindView(tv_prev_close_value)
    TextView tvPrevClose;
    @BindView(tv_open_value)
    TextView tvOpen;
    @BindView(tv_day_high_value)
    TextView tvHigh;
    @BindView(tv_day_low_value)
    TextView tvLow;
    @BindView(tv_52week_high_value)
    TextView tvYearHigh;
    @BindView(tv_52week_low_value)
    TextView tvYearLow;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        // Anu: I think this is to get the caret button up
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // ref: https://guides.codepath.com/android/Google-Play-Style-Tabs-using-TabLayout
        // ref: http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/
        // create a tablayout
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        // Get the view pager and set its PagerAdapter so it can display items
       viewPager = (ViewPager) findViewById(R.id.viewpager);
        // create an adapter for the view pager
        adapter = new HistoryFragmentPagerAdapter(getSupportFragmentManager(),
            DetailActivity.this);
        // attach the adapter to viewPager
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);



        // The Activity (which implements the LoaderCallbacks<Cursor>
        // interface) is the callbacks object through which we will interact
        // with the LoaderManager. The LoaderManager uses this object to
        // instantiate the Loader and to notify the client when data is made
        // available/unavailable.
        mCallbacks = this;
        Uri path = this.getIntent().getData();
        mUri = path;

        String stockFromUri = Contract.Quote.getStockFromUri(mUri);
        Log.d(LOG_TAG, " stock from Uri: " + stockFromUri);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Initialize the Loader with id and callbacks 'mCallbacks'.
        // If the loader doesn't already exist, one is created. Otherwise,
        // the already created Loader is reused. In either case, the
        // LoaderManager will manage the Loader across the Activity/Fragment
        // lifecycle, will receive any new loads once they have completed,
        // and will report this new data back to the 'mCallbacks' object.
        getLoaderManager().initLoader(DETAIL_WEEK_LOADER, null,mCallbacks);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        Loader<Cursor> cursorLoader = null;
        if (null != mUri) {
            switch (loaderId) {
                case DETAIL_WEEK_LOADER: {
                    cursorLoader = new CursorLoader(this, mUri, DETAIL_COLUMNS, null, null, null);
                    break;
                }
                default:{
                    Log.d(LOG_TAG," no cursor available");
                }
            }
        }
        return  cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (!cursor.moveToFirst())
            return;
        int loaderId  = loader.getId();
        switch (loaderId){
            case DETAIL_WEEK_LOADER: {
                DatabaseUtils.dumpCursor(cursor);

                String symbol = cursor.getString(DetailActivity.POSITION_SYMBOL);

               // String percentChange = cursor.getString(DetailActivity.POSITION_ABSOLUTE_CHANGE) +
                 //       " (" + cursor.getString(DetailActivity.POSITION_PERCENTAGE_CHANGE) +
                   //     ")";
                String name = cursor.getString(DetailActivity.POSITION_NAME);
                tvNameSymbol.setText("Name " + " " + name + " "  + symbol + " " );

                tvCurrentPrice.setText("Price " + cursor.getString(DetailActivity.POSITION_PRICE) + " "+
                        cursor.getString(DetailActivity.POSITION_ABSOLUTE_CHANGE) +
                        " (" + cursor.getString(DetailActivity.POSITION_PERCENTAGE_CHANGE) +
                        ")");

                // prev close value is stored as an int in cursor
                // to get the BigDecimal back for display, convert the int back to big decimal and
                // set it
                BigDecimal bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_PREV_CLOSE));
                tvPrevClose.setText(bd.toString());


                bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_OPEN));
                tvOpen.setText(bd.toString());

                bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_DAY_HIGH));
                tvOpen.setText(bd.toString());

                bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_DAY_HIGH));
                tvHigh.setText(bd.toString());

                bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_DAY_LOW));
                tvLow.setText(bd.toString());

                bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_YEAR_HIGH));
                tvYearHigh.setText(bd.toString());

                bd = convertIntToBigDecimal(cursor.getInt(DetailActivity.POSITION_YEAR_LOW));
                tvYearLow.setText(bd.toString());

                adapter.notifyDataSetChanged();

            }
        }


    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private BigDecimal convertIntToBigDecimal(int given) {
        BigDecimal bd = new BigDecimal(given);
        bd = bd.scaleByPowerOfTen(-2);
        return bd;
    }
}
