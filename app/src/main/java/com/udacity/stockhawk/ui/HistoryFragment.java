package com.udacity.stockhawk.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.udacity.stockhawk.R;
import com.udacity.stockhawk.data.History;
import com.udacity.stockhawk.data.PrefUtils;
import com.udacity.stockhawk.utility.MyXAxisValueFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by Anu on 3/5/17.
 *
 * ref: https://github.com/PhilJay/MPAndroidChart/blob/master/MPChartExample/src/com/
 * xxmassdeveloper/mpchartexample/LineChartActivity1.java
 */

// This fragment is to show the chart. Based on the tabs selected this will show the respective chart
public class HistoryFragment extends Fragment
    implements OnChartGestureListener,OnChartValueSelectedListener{

    public static final String PAGE_NUM = "pagenum";
    public static final String HISTORY_OBJECT_KEY = "object";
    public static String LOG_TAG = HistoryFragment.class.getSimpleName();
    private List<Entry> entryList = new ArrayList<>();
    LineChart lineChart;

    private int mPage;
    private History historyModel;

    public HistoryFragment(){

    }

    // This will set the page num for the fragment.
    // called from the pager adapter
 /*   public static HistoryFragment newInstance(int page) {
        HistoryFragment fragment = new HistoryFragment();
        Log.d(LOG_TAG,"inside new Instance setting arguments");

        Bundle args = new Bundle();
        args.putInt("page_num", page);

        fragment.setArguments(args);
        return fragment;
    }
*/
    // This event fires 1st, before the creation of the fragment or any views
    // This method is called when the fragment instance is associated with the activity
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Utils.init(context);
        Log.d(LOG_TAG," on attach()");
    }

    // called when the fragment instance is created/re-created but does not require
    // the activity to be fully created
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG," on create ()");
        // get the tab that is selected
        Bundle args = getArguments();
        mPage = args != null ? args.getInt(PAGE_NUM) : 1;
        historyModel = args.getParcelable(HISTORY_OBJECT_KEY);
        Log.d(LOG_TAG, " history Model half yearly = " + historyModel.getHistoryHalfYearly());


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // inflate the view for the respective tab.
        // need more work
        Log.d(LOG_TAG,"inside onCreateView");
        lineChart = (LineChart) inflater.inflate(R.layout.fragment_detail, container, false);
        Log.d(LOG_TAG, "root view = " + lineChart.toString());

     //   lineChart = (LineChart) rootView.findViewById(R.id.line_chart);
       // Log.d(LOG_TAG, "root view = " + rootView.toString());


        MyMarkerView myMarkerView = new MyMarkerView(getContext(),R.layout.custom_marker_view);
        myMarkerView.setChartView(lineChart);
        lineChart.setMarker(myMarkerView);


        // enable touch gestures
        lineChart.setTouchEnabled(true);

        lineChart.setOnChartGestureListener(this);
        lineChart.setOnChartValueSelectedListener(this);

        // try  both true and false
        lineChart.setDrawGridBackground(true);
        // description test both
        lineChart.getDescription().setEnabled(false);


        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);

        lineChart.setBackgroundColor(Color.TRANSPARENT);

        if (mPage == 2)
            createHistoryDataSetForChart(historyModel.getHistoryQuarterly());
        if (mPage == 1){
            createHistoryDataSetForChart(historyModel.getHistoryHalfYearly());
        }

        return lineChart;
    }

    // When the loader finished getting the cursor history data, this method
    // is called to display the chart
    public void createHistoryDataSetForChart(String history){
        Log.d(LOG_TAG,"history string from sync job = " + history);

        float x = 0, y = 0;
        // split the history string into single line
        // each line contains: timestamp (long), price(float)
        String[] arrayHistory = history.split("\n");

        // This array is for x axis labels: 2015-02-01
       // float[] floatXLabels = new float[arrayHistory.length];

        int i = 0;

        for (String value: arrayHistory) {
            Log.d(LOG_TAG, "timeList  = " + value);
            Log.d(LOG_TAG, " x = " + value.split(",")[0].trim()); // date in millisec
            Log.d(LOG_TAG, " y = " + value.split(",")[1]); // price in float
            try {
                // split strings from history
                String xDateValue = value.split(",")[0].trim(); // in millisec
                String price = value.split(",")[1].trim();

                // to convert long to formatted date: yyyy-MM-dd
                // xDateValue in millisec
                long dateInLong = Long.parseLong(xDateValue);
                String formattedDate = PrefUtils.getFormattedStringDateFromMillisec(dateInLong);

                // add time and price to entry: Entry from the library is of the
                // orm Entry(float x,float y)
                x = Float.valueOf(xDateValue.trim()).floatValue();
                y = Float.valueOf(price).floatValue();

                i++;

            }catch (NumberFormatException exception){
                Log.d(LOG_TAG," Number format exception = " + exception.getMessage());
            }
            // add each x and y to Entry class. This class is used by the chart library
            Entry entry = new Entry(x,y);
            entryList.add(entry);

        }
        for (Entry e: entryList){
            Log.d(LOG_TAG, " list : x  " + e.getX()  + "y = " + e.getY());
        }
        Collections.reverse(entryList);
       /* for (Entry e: entryList){
            Log.d(LOG_TAG, " list : x  " + e.getX()  + "y = " + e.getY());
        }*/
        displayChart();

    }

    private void displayChart() {

        LineDataSet lineDataSet = new LineDataSet(entryList, "StockHistory");
        Log.d(LOG_TAG,"line data set = " + lineDataSet);
        lineDataSet.setColor(Color.GREEN);
        lineDataSet.setValueTextColor(Color.BLUE);

        LineData lineData = new LineData(lineDataSet);
        Log.d(LOG_TAG,"line data  = " + lineData);
        lineChart.setData(lineData);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new MyXAxisValueFormatter());
        xAxis.setLabelCount(5); //  default is 6. min is 2, max is 25.
        // true means you are forcing to have only 6 labels
        //xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);

        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setLabelCount(5);
        yAxis.setDrawAxisLine(false);
      //  yAxis.setDrawGridLines(true);

        lineDataSet.setDrawValues(true);
        lineChart.invalidate(); // refresh

    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            lineChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }


    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }


    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + lineChart.getLowestVisibleX() + ", high: " + lineChart.getHighestVisibleX());
        Log.i("MIN MAX", "xmin: " + lineChart.getXChartMin() + ", xmax: " +
                lineChart.getXChartMax() + ", ymin: " + lineChart.getYChartMin() + ", ymax: "
                + lineChart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }


}
