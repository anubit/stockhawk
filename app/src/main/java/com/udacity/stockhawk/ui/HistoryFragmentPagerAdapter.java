package com.udacity.stockhawk.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.udacity.stockhawk.data.History;
import com.udacity.stockhawk.sync.QuoteSyncJob;

/**
 * Created by Anu on 3/20/17.
 */
// ref: http://stackoverflow.com/questions/10849552/update-viewpager-dynamically?rq=1
// this class will set the titles on the tabs and pass the title of the tab to DetailFragment
public class HistoryFragmentPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private String tabTitles[] = new String[] { "Week", "Month","3M", "6M" };
    private Context context;


    public HistoryFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        this.mNumOfTabs = tabTitles.length;

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        Fragment historyFragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(HistoryFragment.PAGE_NUM,position+1);
        args.putParcelable(HistoryFragment.HISTORY_OBJECT_KEY,QuoteSyncJob.historyObjectModel);
        historyFragment.setArguments(args);
        return historyFragment;

    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof HistoryFragment)
            return super.getItemPosition(object);
         else return POSITION_NONE;
    }

    // This returns the page title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }


}